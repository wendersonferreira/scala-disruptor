name := "scala-disruptor"

version := "0.1.0-ALPHA.1-SNAPSHOT"

organization := "Trustsystems"

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

scalaVersion := "2.10.7"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.3" % Test
