package br.com.trustsystems.disruptor

import scala.annotation.tailrec

object ScalaDisruptor extends App {
  for (n <- 0 to 300) {
    println(s"The fibonacci number at $n is ${fib(n)}")
  }

  def fib(n: Int): Int = {
    if (n < 0)
      throw new IllegalArgumentException("Can't be less than 0")

    @tailrec
    def recursion(n: Int, a: Int, b: Int): Int = n match {
      case 0 => a
      case _ => recursion(n - 1, b, (a + b) % 10000)
    }

    recursion(n % 1500000, 0, 1)
  }
}
