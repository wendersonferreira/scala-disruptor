package br.com.trustsystems.disruptor

import org.scalacheck.{Prop, Properties}

object ScalaDisruptorProps extends Properties("ScalaDisruptor") {
  property("fib") = Prop.forAll { n: Int =>
    lazy val result = ScalaDisruptor.fib(n)
    if (n < 0)
      Prop.throws(classOf[IllegalArgumentException]) {
        result
      }
    else
      result < 10000
  }
}
